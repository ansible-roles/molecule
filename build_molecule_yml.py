#!/usr/bin/env python
import yaml

with open("meta/main.yml", 'r') as stream:
    try:
        platforms = yaml.safe_load(stream)["galaxy_info"]["platforms"]
        if len(platforms) > 0:
            with open(r'./molecule/default/molecule.yml', 'w') as file:
                header = open("./molecule/molecule_yml_header","r")
                file.writelines(header)
                file.close()
            for platform in range(len(platforms)):
                for version in range(len(platforms[platform]["versions"])):
                    with open(r'./molecule/default/molecule.yml', 'a') as file:
                        platform_info = "  - name: ${UUID}-" + platforms[platform]["name"].lower() + str(platforms[platform]["versions"][version]).replace('.', '') + "\n"
                        if platforms[platform]["name"].lower() == "rhel":
                            platform_info += "    user: cloud-user\n"
                        else:
                            platform_info += "    user: " + platforms[platform]["name"].lower() + "\n"
                        platform_info += "    image: " + platforms[platform]["name"].lower() + str(platforms[platform]["versions"][version]).replace('.', '') + "-upstream\n"
                        if platforms[platform]["name"].lower() == "ubuntu":
                           platform_info += "    box: ubuntu/bionic64\n" 
                        elif platforms[platform]["name"].lower() == "rhel":
                           platform_info += "    box: generic/rhel" + str(platforms[platform]["versions"][version]).replace('.', '') + "\n" 
                        else:
                            platform_info += "    box: " + platforms[platform]["name"].lower() + "/" + str(platforms[platform]["versions"][version]).replace('.', '') + "\n"
                        platform_info += "    flavor: micro\n\n"
                        file.writelines(platform_info)
                        file.close()
            with open(r'./molecule/default/molecule.yml', 'a') as file:
                footer = open("./molecule/molecule_yml_footer","r")
                file.writelines(footer)
                file.close()
    except yaml.YAMLError as exc:
        print(exc)
