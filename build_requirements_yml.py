#!/usr/bin/env python
import yaml
import os

with open("meta/main.yml", 'r') as stream:
    try:
        dependencies = yaml.safe_load(stream)["dependencies"]
        if len(dependencies) > 0:
            with open('./molecule/default/requirements.yml', 'w') as file:
                file.writelines("---\n")
                yaml.dump(dependencies, file)
                file.writelines(["\n",\
                                 "- name: rhsm\n",\
                                 "  src: ssh://git@code.vt.edu/ansible-roles/rhsm.git\n",\
                                 "  scm: git\n"])
                file.close()
            print(open('./molecule/default/requirements.yml','r').read())
        else:
            with open('./molecule/default/requirements.yml', 'w') as file:
                file.writelines(["---\n",\
                                 "- name: rhsm\n",\
                                 "  src: ssh://git@code.vt.edu/ansible-roles/rhsm.git\n",\
                                 "  scm: git\n"])
                file.close()
            print(open('./molecule/default/requirements.yml','r').read())
    except yaml.YAMLError as exc:
        print(exc)
