#!/usr/bin/env python
import os.path

if os.path.isfile('./pipeline/test_default.py'):
    os.replace('./pipeline/test_default.py', './molecule/resources/tests/test_default.py')
